from named_fields import *


def _polynom_value(poly, x, field=rational):
    value = field.zero()
    for coeff in reversed(poly):
        value *= x
        value += coeff
    return value


def _make_random_shares(minimum, shares, field=rational, secret=None):
    if minimum > shares:
        raise ValueError("pool secret would be irrecoverable")
    poly = [field.rand(True) for i in range(minimum)]
    if secret is not None:
        if secret < 0 or field.characteristic != 0 and secret >= field.characteristic:
            raise ValueError("secret out of field")
        poly[0] = secret
    points = [(field(i), _polynom_value(poly, field(i), field))
              for i in range(1, shares + 1)]
    return poly[0], points, poly


def make_random_shares(minimum, shares, field=rational, secret=None):
    secret, shares, poly = _make_random_shares(minimum, shares, field, secret)
    return secret, shares


def _lagrange_interpolate(x, x_s, y_s, field=rational):
    k = len(x_s)
    assert k == len(set(x_s)), "points must be distinct"
    def product(vals):
        value = field.one()
        for v in vals:
            value *= v
        return value
    nums = []
    dens = []
    for i in range(k):
        others = list(x_s)
        cur = others.pop(i)
        nums.append(product(x - o for o in others))
        dens.append(product(cur - o for o in others))
    den = product(dens)
    num = sum([nums[i] * den * y_s[i] / dens[i] for i in range(k)])
    return num / den


def recover_secret(shares, field=rational):
    try:
        x_s, y_s = zip(*shares)
    except ValueError:
        x_s, y_s = [], []
    return _lagrange_interpolate(field.zero(), x_s, y_s, field)
