from arithmetic import Field, FieldElem, Polynom, mulinv
from fractions import Fraction
import random


class RationalField(Field):
    def __call__(self, value):
        return FieldElem(self, Fraction(value))

    def zero(self):
        return FieldElem(self, Fraction())

    def add(self, a, b):
        return a + b

    def neg(self, a):
        return -a

    def one(self):
        return FieldElem(self, Fraction(1))

    def mul(self, a, b):
        return a * b

    def inv(self, a):
        return 1 / a

    def rand(self, non_zero=False):
        r = random.gauss(0, 1000)
        while non_zero and r == 0:
            r = random.gauss(0, 1000)
        return self.__call__(r)

    def value(self, val):
        return int(val)

    def __repr__(self):
        return "Q"


class GaloisPrimeField(Field):
    def __init__(self, prime):
        self._prime = prime
        self.characteristic = prime

    def __call__(self, value):
        return FieldElem(self, int(value) % self._prime)

    def zero(self):
        return FieldElem(self, 0)

    def add(self, a, b):
        return (a + b) % self._prime

    def neg(self, a):
        return -a % self._prime

    def one(self):
        return FieldElem(self, 1)

    def mul(self, a, b):
        return (a * b) % self._prime

    def inv(self, a):
        if a == 0:
            raise ZeroDivisionError
        return mulinv(a, self._prime)

    def rand(self, non_zero=False):
        return self.__call__(random.randint(1 if non_zero else 0, self.characteristic))

    def value(self, val):
        return val

    def __repr__(self):
        return f"GF({self._prime})"


class GaloisExtensionField(Field):
    def __init__(self, prime, power, polynom):
        self._prime = prime
        self._power = power
        self._polynom = Polynom(polynom)
        self.characteristic = prime ** power

    def __call__(self, value):
        if isinstance(value, int):
            value = self._convert(value)
        value = Polynom([int(elem) % self._prime for elem in value] + (self._power - len(value)) * [0])
        return FieldElem(self, value)

    def zero(self):
        return FieldElem(self, self._canonical([]))

    def add(self, a, b):
        return self._canonical(a + b)

    def neg(self, a):
        return self._canonical(-a)

    def one(self):
        return FieldElem(self, self._canonical([1]))

    def mul(self, a, b):
        return self._canonical((a * b) % self._polynom)

    def inv(self, a):
        if a == self._canonical([]):
            raise ZeroDivisionError
        return self._canonical(mulinv(a, self._polynom, zero=lambda: self._canonical([]), one=lambda: self._canonical([1]), mod=lambda x: x.elem_mod(self._prime)))

    def rand(self, non_zero=False):
        value = [random.randint(1 if non_zero else 0, self._prime) for i in range(self._power)]
        return self.__call__(value)

    def value(self, val):
        result = 0
        for coeff in reversed(val):
            result *= self._prime
            result += coeff
        return result

    def _convert(self, idx):
        tmp = []
        while idx != 0:
            tmp.append(idx % self._prime)
            idx //= self._prime
        return tmp

    def _canonical(self, value):
        return Polynom(value[:self._power] + (self._power - len(value)) * [0]).elem_mod(self._prime)

    def __repr__(self):
        return f"GF({self._prime}**{self._power}/{self._polynom})"
