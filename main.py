#!/usr/bin/env python3
import itertools
import sys, getopt
import shamir
import named_fields
import conversion
import base62
import re


def powerset(iterable):
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s)+1))


def shamir_test(m, s, field=named_fields.rational, secret=None, codec=conversion.Codec()):
    codec.set_field(field)
    secret_elem, shares, poly = shamir._make_random_shares(minimum=m, shares=s, field=field, secret=codec.encode(secret))
    
    correct_answer = secret_elem() if secret is None else secret
    
    print(f"Polynom: {poly}")
    print(f"Secret Element: {secret_elem}")
    print(f"Shares: {shares}")
    for idxs in powerset(range(s)):
        combination = [shares[idx] for idx in idxs]
        recovered = codec.decode(shamir.recover_secret(combination, field)())
        ok = "✓ " if recovered == correct_answer else "✗ "
        print(ok + f"Combination: {idxs} Value: {recovered}")


def test():
    print("Q (rational numbers):")
    shamir_test(3, 4)
    print()
    print("F_280829369862134719390036617067 (modular numbers):")
    shamir_test(3, 4, named_fields.gf30)
    print()
    print("F_256 Rijndael (polynomials):")
    shamir_test(3, 4, named_fields.rijndael)


def encode_secret(threshold, total, secret, field=named_fields.gf521):
    random_padder = conversion.RandomPaddedStringCodec()
    random_padder.set_field(field)
    _, shares = shamir.make_random_shares(threshold, total, field, random_padder.encode(secret))
    return shares


def decode_secret(shares, field=named_fields.gf521):
    random_padder = conversion.RandomPaddedStringCodec()
    random_padder.set_field(field)
    recovered = random_padder.decode(shamir.recover_secret(shares, field)())
    return recovered


def prompt(prompt_message='Enter data: ', type_=str, validator=lambda value: True, prompt_after_error=None):
    if prompt_after_error is None:
        prompt_after_error = prompt_message

    try:
        read = input(prompt_message)
        while True:
            try:
                value = type_(read)
                if not validator(value):
                    raise ValueError
                return value
            except (TypeError, ValueError):
                read = input(prompt_after_error)
    except (EOFError, KeyboardInterrupt):
        print()
        raise SystemExit(1)


def do_encryption(field=named_fields.gf30):
    total = prompt('Number of shares: ', int, lambda val: val > 0, 'Bad value! Enter a positive integer: ')
    threshold = prompt(f'Threshold (smaller than {total}): ', int, lambda val: 0 < val <= total, 'Bad value! Enter a positive integer in the given range: ')
    secret = prompt('The secret (not too long): ')
    while True:
        try:
            shares = encode_secret(threshold, total, secret, field)
            break
        except ValueError:
            secret = prompt('Bad, it was too long! Try again with a shorter secret: ')
    print('Shares:')
    for share in shares:
        print('_'.join(map(lambda v: base62.encode(v()), share)))


def do_decryption(field=named_fields.gf30):
    def parse(str_, fn=field):
        return tuple(map(lambda v: fn(base62.decode(v)), str_.split('_')))

    def check(val):
        return len(val) == 2

    splitter = re.compile('[^0-9a-zA-Z_]+')

    print('Enter the shares:')
    while True:
        shares = []
        try:
            while True:
                try:
                    input_ = input()
                    if input_ == '':
                        break
                    for share in splitter.split(input_):
                        share = parse(share)
                        if not check(share):
                            raise ValueError
                        shares.append(share)
                except EOFError:
                    break

            if len(shares) == 0:
                raise SystemExit(1)
            print('Potentially recovered secret:')
            recovered = decode_secret(shares, field)
            print(recovered)
            break
        except ValueError:
            print('Bad, some of the shares were invalid! Try again:')
        except KeyboardInterrupt:
            raise SystemExit(1)


def main(argv):
    help_message = '''\
Parameters:
--encrypt (-e)
--decrypt (-d)
--test (-t)\
'''

    try:
        opts, args = getopt.getopt(argv, 'edth', ['encrypt', 'decrypt', 'test', 'help'])
    except getopt.GetoptError:
        print(help_message)
        raise SystemExit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(help_message)
        elif opt in ('-e', '--encrypt'):
            do_encryption(named_fields.gf521)
        elif opt in ('-d', '--decrypt'):
            do_decryption(named_fields.gf521)
        elif opt in ('-t', '--test'):
            test()
        break
    else:
        choice = prompt('Choose [e]ncryption or [d]ecryption: ', lambda val: val.lower(), lambda str_: any(map(lambda x: x.startswith(str_), ['encryption', 'decryption'])))
        if choice.startswith('e'):
            do_encryption(named_fields.gf521)
        else:
            do_decryption(named_fields.gf521)


if __name__ == '__main__':
    main(sys.argv[1:])
