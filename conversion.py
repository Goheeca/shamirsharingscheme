import random


class Codec(object):
    def __init__(self):
        self._field = None

    def set_field(self, field):
        self._field = field

    def encode(self, input):
        return input

    def decode(self, output):
        return output


class StringCodec(Codec):
    def encode(self, input):
        conv = input.encode('utf-8')
        res = 0
        for byte in reversed(conv):
            res *= 256
            res += byte
        return res

    def decode(self, output):
        conv = []
        while output != 0:
            conv.append(output % 256)
            output //= 256
        conv = bytes(conv)
        return conv.decode('utf-8', 'replace')


class RandomPaddedStringCodec(StringCodec):
    def encode(self, input):
        input = super().encode(input)
        exp = 0
        while 256**exp < input:
            exp += 1
        exp += 1 # zero byte
        input_ = input
        while self._field.characteristic > input_:
            input = input_
            input_ += random.randint(0, 255) * 256**exp
            exp += 1
        return input

    def decode(self, output):
        output = super().decode(output)
        try:
            output = output[:output.index('\x00')]
        except ValueError:
            pass
        return output
